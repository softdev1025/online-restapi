from flask import Flask, request, jsonify

from mmapi import utils
from mmcore import redistools
from mmcore.models.api import Keys


app = Flask(__name__)


@app.errorhandler(404)
def error_404_json(exception):
    return utils.error(
        'Invalid endpoint! View our docs at http://docs-new.macromeasures.com/'
    )


MODES = {
    'email': ('fullcontact', True),
    'hashed_email': ('fullcontact', False),
    'twitter_username': ('twitter', True),
    'twitter_id': ('twitter', False),
    'instagram_username': ('instagram', True),
    'instagram_id': ('instagram', False),
}
@app.route('/<mode>/profiles.json')
def profiles_json(mode):
    if mode not in MODES:
        return utils.error(
            "Invalid mode %s - must be one of %s" % (
                mode,
                '/'.join(MODES.keys())
            )
        )

    service, username_input = MODES[mode]

    inputs = request.args.get('v', '')
    inputs = [u for u in inputs.split(',') if u]
    if not inputs:
        return utils.error("No inputs specified")

    key = request.args.get('key')
    if not key:
        return utils.error("No API key specified")

    api_key = Keys.get(key=key)
    if not key:
        return utils.error("Invalid API key: %s" % key)

    return utils.get_profiles(service, inputs, api_key, username_input)


@app.route('/<service>/users.json')
def users_json(service):
    if service == 'email':
        # TODO: Force merging profiles if email is used
        service = 'fullcontact'

    user_ids = request.args.get('ids', '')
    user_inputs = [u for u in user_ids.split(',') if u]
    use_usernames = False
    if not user_ids:
        usernames = request.args.get('usernames', '')
        user_inputs = [u for u in usernames.split(',') if u]
        use_usernames = True

    cutoff = request.args.get('cutoff', type=int)

    key = request.args.get('key')
    if key:
        api_key = Keys.get(key=key)
    else:
        api_key = None

    return utils.get_response_for_request(
        service,
        user_inputs,
        api_key,
        cutoff,
        use_usernames,
    )


@app.route('/limits.json')
def limits_json():
    key = request.args.get('key')
    if key is None:
        return utils.error('No API key specified')

    api_key = Keys.get(key=key)
    if api_key is None:
        return utils.error('Invalid API key specified: %s' % key)

    limits = {}
    for service in api_key['services']:
        key_limits = api_key['services'][service]

        period_limit = key_limits['requests_per_period']
        period_total = redistools.PeriodKey(
            service=service, key=key
        ).get_total()
        if period_total:
            period_remaining = period_limit - int(period_total)
        else:
            period_remaining = period_limit

        seconds_remaining = redistools.PeriodKey(
            service=service, key=key
        ).get_remaining()
        if seconds_remaining <= 0:
            seconds_remaining = key_limits['seconds_per_period']

        lifetime_limit = key_limits['lifetime_limit']
        lifetime_total = redistools.TotalKey(
            service=service, key=key
        ).get()
        if lifetime_limit > 0:
            lifetime_remaining = lifetime_limit - lifetime_total
        else:
            lifetime_remaining = 0

        limits[service] = {
            'period_limit': period_limit,
            'period_remaining': period_remaining,
            'period_reset': seconds_remaining,
            'lifetime_limit': lifetime_limit,
            'lifetime_remaining': lifetime_remaining,
        }

    return jsonify(limits)

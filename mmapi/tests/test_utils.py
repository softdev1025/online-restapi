import unittest

import mock

from mmapi import constants
from mmapi.utils import get_response_for_request


@mock.patch('mmapi.utils.labeller')
@mock.patch('mmapi.utils.jsonify')
@mock.patch('mmapi.utils.redistools')
class TestUsers(unittest.TestCase):
    def setUp(self):
        self.api_key = mock.MagicMock()
        api_key_data = {
            'key': 'key123',
            'services': {
                'twitter': {
                    'lifetime_total': 0,
                    'lifetime_limit': 0,
                    'requests_per_period': 0,
                    'seconds_per_period': 0,
                },
            }
        }
        self.api_key.__getitem__.side_effect = api_key_data.__getitem__

        self.response = mock.Mock()
        self.response.headers = {}

    def test_invalid_service(self, mock_redis, mock_jsonify, mock_labeller):
        response = get_response_for_request(
            'fake', ['10'], ['gender'], self.api_key
        )
        mock_jsonify.assert_called_once_with({
            'error': True,
            'message': constants.INVALID_SERVICE
        })
        self.assertFalse(mock_labeller.get_labels.called)

    def test_no_api_key(self, mock_redis, mock_jsonify, mock_labeller):
        response = get_response_for_request(
            'twitter', ['10'], ['gender'], None
        )
        mock_jsonify.assert_called_once_with({
            'error': True,
            'message': constants.NO_API_KEY
        })
        self.assertFalse(mock_labeller.get_labels.called)

    def test_no_attributes(self, mock_redis, mock_jsonify, mock_labeller):
        response = get_response_for_request(
            'twitter', ['10'], [], self.api_key
        )
        mock_jsonify.assert_called_once_with({
            'error': True,
            'message': constants.NO_ATTRIBUTES
        })
        self.assertFalse(mock_labeller.get_labels.called)

    def test_no_users(self, mock_redis, mock_jsonify, mock_labeller):
        response = get_response_for_request(
            'twitter', [], ['gender'], self.api_key
        )
        mock_jsonify.assert_called_once_with({
            'error': True,
            'message': constants.NO_USERS
        })
        self.assertFalse(mock_labeller.get_labels.called)

    def test_hit_lifetime_limit(self, mock_redis, mock_jsonify, mock_labeller):
        """Test when there are not enough lifetime calls remaining to service
        the request, so the call should be blocked immediately."""
        self.api_key['services']['twitter']['lifetime_limit'] = 100
        mock_redis.TotalKey.return_value.increment.return_value = 101
        response = get_response_for_request(
            'twitter', ['123'], ['gender'], self.api_key
        )

        mock_redis.TotalKey.return_value.increment.assert_called_once_with(1)

        # It should return an error message.
        mock_jsonify.assert_called_once_with({
            'error': True,
            'message': constants.HIT_LIFETIME_LIMIT,
        })
        # Should not call get_labels, and should decrement the lifetime total.
        self.assertFalse(mock_labeller.get_labels.called)
        mock_redis.TotalKey.return_value.decrement.assert_called_once_with(1)

    def test_hit_period_limit(self, mock_redis, mock_jsonify, mock_labeller):
        """Test when the lifetime limit hasn't been exceeded, but there aren't
        enough period requests remaining to service this request."""
        self.api_key['services']['twitter']['lifetime_limit'] = 10000
        self.api_key['services']['twitter']['requests_per_period'] = 100
        self.api_key['services']['twitter']['seconds_per_period'] = 60
        mock_redis.TotalKey.return_value.increment.return_value = 101
        mock_redis.PeriodKey.return_value.increment.return_value = 101

        response = get_response_for_request(
            'twitter', ['100', '200'], ['gender'], self.api_key
        )

        mock_redis.TotalKey.return_value.increment.assert_called_once_with(2)
        mock_redis.PeriodKey.return_value.increment.assert_called_once_with(
            2, 60
        )
        mock_jsonify.assert_called_once_with({
            'error': True,
            'message': constants.HIT_PERIOD_LIMIT,
        })
        self.assertFalse(mock_labeller.get_labels.called)

    def test_first_call_incomplete(self, mock_redis, mock_jsonify,
                                   mock_labeller):
        """Test the first time a particular API key makes a request. The
        requested user has not yet been classified."""
        self.api_key['services']['twitter']['lifetime_limit'] = 100
        self.api_key['services']['twitter']['requests_per_period'] = 100
        self.api_key['services']['twitter']['seconds_per_period'] = 60
        mock_redis.TotalKey.return_value.increment.return_value = 1
        mock_redis.PeriodKey.return_value.increment.return_value = 1
        mock_labeller.get_labels.return_value = None

        response = get_response_for_request(
            'twitter', ['123'], ['gender'], self.api_key
        )

        mock_redis.TotalKey.return_value.increment.assert_called_once_with(1)
        mock_redis.PeriodKey.return_value.increment.assert_called_once_with(
            1, 60
        )
        mock_redis.TotalKey.return_value.decrement.assert_called_once_with(1)
        mock_redis.PeriodKey.return_value.decrement.assert_called_once_with(1)
        mock_jsonify.assert_called_once_with({
            'error': False,
            'complete': False,
        })
        mock_labeller.get_labels.assert_called_once_with(
            'twitter', ['123'], ['gender'], self.api_key,
        )

    def test_first_call_complete(self, mock_redis, mock_jsonify,
                                 mock_labeller):
        """Test the first time a particular API key makes a request, with the
        user being already done."""
        self.api_key['services']['twitter']['lifetime_limit'] = 1000
        self.api_key['services']['twitter']['requests_per_period'] = 100
        self.api_key['services']['twitter']['seconds_per_period'] = 60
        mock_redis.TotalKey.return_value.increment.return_value = 1
        mock_redis.PeriodKey.return_value.increment.return_value = 1
        mock_redis.PeriodKey.return_value.get_remaining.return_value = 60
        mock_labeller.get_labels.return_value = {
            '123': 'stuff'
        }
        mock_jsonify.return_value = self.response

        response = get_response_for_request(
            'twitter', ['123'], ['gender'], self.api_key
        )

        mock_redis.TotalKey.return_value.increment.assert_called_once_with(1)
        self.assertFalse(mock_redis.TotalKey.return_value.decrement.called)
        self.assertFalse(mock_redis.PeriodKey.return_value.decrement.called)
        mock_jsonify.assert_called_once_with({
            'error': False,
            'complete': True,
            'labels': {
                '123': 'stuff',
            }
        })
        self.api_key.save.assert_called_once_with()

        self.assertEqual(response.headers, {
            'X-RateLimit-PeriodLimit': 100,
            'X-RateLimit-PeriodRemaining': 99,
            'X-RateLimit-PeriodReset': 60,
            'X-RateLimit-LifetimeLimit': 1000,
            'X-RateLimit-LifetimeRemaining': 999,
        })

    def test_last_lifetime_call(self, mock_redis, mock_jsonify, mock_labeller):
        self.api_key['services']['twitter']['lifetime_limit'] = 100
        self.api_key['services']['twitter']['requests_per_period'] = 100
        self.api_key['services']['twitter']['seconds_per_period'] = 60
        mock_redis.TotalKey.return_value.increment.return_value = 100
        mock_redis.PeriodKey.return_value.increment.return_value = 100
        mock_redis.PeriodKey.return_value.get_remaining.return_value = 0
        mock_labeller.get_labels.return_value = {
            '123': 'stuff',
        }

        mock_jsonify.return_value = self.response

        response = get_response_for_request(
            'twitter', ['123'], ['gender'], self.api_key
        )

        mock_redis.TotalKey.return_value.increment.assert_called_once_with(1)
        self.assertFalse(mock_redis.TotalKey.return_value.decrement.called)
        self.assertFalse(mock_redis.PeriodKey.return_value.decrement.called)
        mock_jsonify.assert_called_once_with({
            'error': False,
            'complete': True,
            'labels': {
                '123': 'stuff',
            }
        })
        self.api_key.save.assert_called_once_with()

        self.assertEqual(response.headers, {
            'X-RateLimit-PeriodLimit': 100,
            'X-RateLimit-PeriodRemaining': 0,
            'X-RateLimit-PeriodReset': 0,
            'X-RateLimit-LifetimeLimit': 100,
            'X-RateLimit-LifetimeRemaining': 0,
        })

from flask import jsonify

from mmapi import constants
from mmcore import redistools, labeller


def error(msg):
    return jsonify({
        'error': True,
        'message': msg,
    })


def get_response_for_request(service, user_inputs, api_key, cutoff,
                             use_usernames):
    if not user_inputs:
        return error(constants.NO_USERS)

    if not api_key:
        return error(constants.NO_API_KEY)

    if service not in api_key['services']:
        return error(constants.INVALID_SERVICE % service)

    key = api_key['key']
    key_limits = api_key['services'][service]
    num_requests = len(user_inputs)

    total_key = redistools.TotalKey(service=service, key=key)
    period_key = redistools.PeriodKey(service=service, key=key)

    # Make sure the lifetime rate limit hasn't been exceeded.
    lifetime_limit = key_limits['lifetime_limit']
    lifetime_total = total_key.increment(num_requests)
    if lifetime_limit and lifetime_total > lifetime_limit:
        total_key.decrement(num_requests)
        return error(constants.HIT_LIFETIME_LIMIT)

    # Make sure the period rate limit hasn't been exceeded.
    seconds_per_period = key_limits['seconds_per_period']
    period_limit = key_limits['requests_per_period']
    period_total = period_key.increment(num_requests, seconds_per_period)
    if period_total > period_limit:
        total_key.decrement(num_requests)
        period_key.decrement(num_requests)
        return error(constants.HIT_PERIOD_LIMIT)

    labels = labeller.get_labels(
        service,
        user_inputs,
        api_key,
        cutoff=cutoff,
        use_usernames=use_usernames,
    )

    if labels is None:
        # The users are not yet done. Remove the current requests from the
        # period and lifetime request counters.
        total_key.decrement(num_requests)
        period_key.decrement(num_requests)
        return jsonify({
            'error': False,
            'complete': False,
        })

    # It's complete!
    response = jsonify({
        'error': False,
        'complete': True,
        'labels': labels
    })

    key_limits['lifetime_total'] = lifetime_total
    api_key.save()

    seconds_remaining = period_key.get_remaining()
    period_remaining = period_limit - period_total
    if lifetime_limit:
        lifetime_remaining = lifetime_limit - lifetime_total
    else:
        lifetime_remaining = 0

    # Add the headers containing rate-limiting information.
    response.headers['X-RateLimit-PeriodLimit'] = period_limit
    response.headers['X-RateLimit-PeriodRemaining'] = period_remaining
    response.headers['X-RateLimit-PeriodReset'] = seconds_remaining
    response.headers['X-RateLimit-LifetimeLimit'] = lifetime_limit
    response.headers['X-RateLimit-LifetimeRemaining'] = lifetime_remaining

    return response

#FROM 573308292692.dkr.ecr.us-east-1.amazonaws.com/macromeasures:mm-core
FROM mm-core

USER root

# Deps for mm-api
COPY requirements.txt /home/appuser/mm-api/requirements.txt
RUN pip install -r /home/appuser/mm-api/requirements.txt

COPY . /home/appuser/mm-api

RUN chown -R appuser:appuser /home/appuser

USER appuser

ENV PYTHONPATH /home/appuser/mm-api/:$PYTHONPATH

EXPOSE 8000

ENTRYPOINT ["gunicorn", "-w", "20", "mmapi.flask_app:app", "--config", "python:mmapi.gunicorn"]
